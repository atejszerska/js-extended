import { Person } from "./person.model";

export interface ProcessedPerson {
  [key: string]: Person
}