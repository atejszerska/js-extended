'use strict';

// L05E03 - TypeScriptowane

const arr1: number[] = [1, 2, 3, 4, 5];
const res1: number = arr1.reduce(
  (acc: number, curVal: number) => acc + curVal,
  0
);

console.log(res1);
