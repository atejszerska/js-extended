let a: string = 'test';
// console.log(a);
// a = +a // Type 'number' is not assignable to type 'string'

const x: number | string | boolean = '5' // albo liczba albo string albo boolean

const b: number = 5;
const c: string = a + b;

const d: any = 'tak'; // cokolwiek

const arr: number[] = [1, 2]; // tablica liczb; [1, 2, '5'] by krzyczało
const arr2: number[] | string[] = [1, 5]; // albo tablica stringów albo tablica liczb
const arr3: (number | string)[] = ['4', 'fff', 7]; // tablica liczb i stringów

function fun1(a: number): number {
  return a;
}

function fun2(a: number, b: number): string {
  return "Wynik: " + (a + b);
}

function nicNieZwracam(): void {
  console.log("Hello World!");
}

// console.log(fun2(5, 6));
// nicNieZwracam();

// const res = nicNieZwracam();
// console.log(res); // undefined


function fun3(status: any): string {
  switch (status) {
    case 1:
      return 'one';
  
    default:
      return 'other';
  }
}
