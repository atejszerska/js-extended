import { UserStatus } from "./enums/user-status.enum";
import { Person } from "./models/person.model";
import { ProcessedPerson } from "./models/processed-person.model";
import { User } from "./models/user.model";
// interfejsy tworzymy w innym pliku i je importujemy (VS Code zaimportuje to za nas)

// wyrzuca błąd, bo brakuje wartości: city, email, date, age
// const user: User = {
//   firstName: 'Jan',
//   lastName: 'Kowalski'
// }

const user: User = {
  firstName: 'Jan',
  lastName: 'Kowalski',
  city: 'Gdańsk',
  email: 'aa@a.pl',
  date: new Date(), // bierzemy aktualną datę
  age: 20,
  status: UserStatus.ACTIVE // odwołujemy się do naszego enum
}

const user2: User = {
  firstName: 'Krzysztof',
  lastName: 'Nowak',
  city: 'Gdynia',
  email: 'krzych@wp.pl',
  status: UserStatus.INACTIVE
}

console.log(user.email);

const users: User[] = [user, user2];


const objects: Person[] = [ 
  { id: 'abc', name: 'Ala' }, 
  { id: 'def', name: 'Tomek' }, 
  { id: 'ghi', name: 'Jan' } 
];

const result: ProcessedPerson[] = objects.reduce(
  (acc: ProcessedPerson[], curVal: Person) => [...acc, {[curVal.id]: curVal}],
  []
);

console.log(result);
