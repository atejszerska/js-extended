const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

const users = [
  {
    id: 1,
    firstName: "Anna",
    lastName: "Nowak",
    status: 'ACTIVE'
  },
  {
    id: 2,
    firstName: "Jan",
    lastName: "Kowalski",
    status: 'ACTIVE'
  }
];

app.get('/api', (req, res) => {
  res.send('Hello World!');
});

app.get('/api/users', (req, res) => {
  res.send(users);
});

app.get('/api/user/:id', (req, res) => {
  const id = +req.params.id;
  const user = users.find((user) => user.id === id);
  res.status(200).send(user);
});

app.post('/api/user', (req, res) => {
  const newId = users[users.length - 1].id + 1;
  const body = req.body;
  const user = {
    id: newId,
    firstName: body.firstName,
    lastName: body.lastName
  };

  users.push(user);
  res.status(200).send(user);
});

app.put('/api/user/:id', (req, res) => {
  // const id = +req.params.id;
  // console.log(id, req.body);
  // todo - do uzupełnienia metoda PUT (update użytkownika)
  res.status(200).send();
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});