import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDto } from '../dto/user-dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient
  ) {}

  getUsers(): Observable<UserDto[]> {
    return this.httpClient.get<UserDto[]>(`/api/users`)
  }

  getUserById(id: number): Observable<UserDto> {
    return this.httpClient.get<UserDto>(`/api/user/${id}`);
  }

  createUser(user: UserDto): Observable<UserDto> {
    return this.httpClient.post<UserDto>(`/api/user`, user);
  }
}
