import { Pipe, PipeTransform } from '@angular/core';
import { Status } from '../enums/status.enum';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: Status): string {
    switch (value) {
      case Status.ACTIVE:
        return 'aktywny';
      case Status.INACTIVE:
        return 'nieaktywny';
      default:
        return '';
    }
  }

}
