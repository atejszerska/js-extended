import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UserDto } from '../../dto/user-dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users!: UserDto[];
  disabledButton = false;

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // new Promise((res, rej) => {
    //   res(true);
    // }).then(() => {}).catch().finally()

    // this.userService.getUsers().subscribe(
    //   (res: UserDto[]) => {
    //     this.users = res;
    //   }, 
    //   () => {

    //   },
    //   () => {

    //   });

    this.disabledButton = true;
    this.userService.getUsers().subscribe({
      next: (users) => {
        this.users = users;
      },
      error: () => {},
      complete: () => {
        this.disabledButton = false;
      }
    });
  }

  goToDetails(id: number) {
    this.router.navigate(['/details', id]);
  }

  addUser(): void {
    this.router.navigate(['/form']);
  }
}
