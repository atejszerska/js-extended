import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  userForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {}

  public ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: [''],
      xyz: []
    });

    console.log(this.userForm);
    this.userForm.get('xyz')?.setValue('ghjk');
    this.userForm.get('xyz')?.disable();

    this.onChanges();
    
  }

  onChanges(): void {
    this.userForm.get('firstName')?.valueChanges.subscribe((data) => {
      console.log(data);
    });
  }

  save(): void {
    console.log(this.userForm.value);
  }
}
