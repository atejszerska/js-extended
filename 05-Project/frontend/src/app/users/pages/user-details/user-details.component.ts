import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { UserDto } from '../../dto/user-dto';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';
import { Status } from '../../enums/status.enum';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  user!: UserDto;

  readonly Status: typeof Status = Status;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // this.route.params.subscribe((params: Params) => {
    //   this.userService.getUserById(params['id']).subscribe((res: UserDto) => {
    //     this.user = res;
    //     console.log(res);
    //   });
    // });

    this.route.data.subscribe((data: Data) => {
      this.user = data['user'];
      console.log(this.user);
    });
  }

  goBack(): void {
    this.router.navigate(['/list']);
  }

  transform(status: Status) {
    switch (status) {
      case Status.ACTIVE:
        return 'aktywny';
      case Status.INACTIVE:
        return 'nieaktywny';
      default:
        return '';
    }
  }
}
