import { Status } from "../enums/status.enum";

export interface UserDto {
  readonly id: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly status: Status;
}