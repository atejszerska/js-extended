import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './users/pages/user-list/user-list.component';
import { UserDetailsComponent } from './users/pages/user-details/user-details.component';
import { HttpClientModule } from '@angular/common/http';
import { StatusPipe } from './users/pipes/status.pipe';
import { UserFormComponent } from './users/pages/user-form/user-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CounterComponent } from './counter/counter/counter.component';
import { CounterDetailsComponent } from './counter/counter-details/counter-details.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailsComponent,
    StatusPipe,
    UserFormComponent,
    CounterComponent,
    CounterDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
