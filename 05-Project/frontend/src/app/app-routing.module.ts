import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDetailsComponent } from './users/pages/user-details/user-details.component';
import { UserListComponent } from './users/pages/user-list/user-list.component';
import { UserResolver } from './users/resolvers/user.resolver';
import { UserFormComponent } from './users/pages/user-form/user-form.component';
import { CounterComponent } from './counter/counter/counter.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list',
  },
  {
    path: 'list',
    component: UserListComponent
  },
  {
    path: 'details/:id',
    component: UserDetailsComponent,
    resolve: { user: UserResolver }
  },
  {
    path: 'form',
    component: UserFormComponent
  },
  {
    path: 'counter',
    component: CounterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
