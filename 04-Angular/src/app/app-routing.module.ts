import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './modules/account/account.component';
import { UsersListComponent } from './modules/users/users-list/users-list.component';
import { UsersDetailsComponent } from './modules/users/users-details/users-details.component';

const routes: Routes = [
  {
    path: 'users-list',
    component: UsersListComponent
  },
  {
    path: 'users-details',
    component: UsersDetailsComponent
  },
  {
    path: 'account',
    component: AccountComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
