import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  value: string = 'Test';
  users: any[] = [];

  ngOnInit(): void {
    // console.log('test', this.value);
    setTimeout(() => {
      this.users = [ 
        {
          firstName: 'Anna',
          lastName: 'Kowalska'
        },
        {
          firstName: 'Hanna',
          lastName: 'Nowak'
        }
      ];
    }, 2000);
  }
}
