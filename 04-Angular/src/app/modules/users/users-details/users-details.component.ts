import { Component } from '@angular/core';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.scss']
})
export class UsersDetailsComponent {
  user: any = {};

  ngOnInit(): void {
    this.user =
      {
        firstName: 'Anna',
        lastName: 'Kowalska'
      }
    }
}
